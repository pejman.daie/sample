
package ....

import BusinessServiceException;
import enums.ErrorCode;
import payment.domain.dto.response.RegistrationPlanDTO;
import domain.dto.UserRegistrationPlanDTO;
import user.infra.registrationplans.MultiplePackageClass;
import user.infra.registrationplans.PeremiumPackageClass;
import user.infra.registrationplans.RegistrationPlansBaseClass;
import user.infra.registrationplans.SinglePackageClass;
import org.springframework.http.HttpStatus;

import java.util.List;

public enum RegistrationPlan {
    SINGLE_PACKAGE(new SinglePackageClass(), 995L),
    MULTIPLE_PACKAGE(new MultiplePackageClass(), 2995L),
    PREMIUM_PACKAGE(new PeremiumPackageClass(), 4995L);

    private RegistrationPlansBaseClass registrationPlanInterface;
    private long price;

    RegistrationPlan(RegistrationPlansBaseClass registrationPlanInterface, long price) {
        this.registrationPlanInterface = registrationPlanInterface;
        this.price = price;
    }

    public static UserRegistrationPlanDTO validatePlan(UserRegistrationPlanDTO userRegistrationPlanDTO) throws BusinessServiceException {
        if (!userRegistrationPlanDTO.isEnabled()) {
            throw new BusinessServiceException(HttpStatus.EXPECTATION_FAILED, ErrorCode.IO_EXCEPTION);
        }
        return RegistrationPlan.valueOf(userRegistrationPlanDTO.getRegistrationPlan().toString()).registrationPlanInterface.validatePlan(userRegistrationPlanDTO);
    }

    public static String getEmailText(String planName) {
        return RegistrationPlan.valueOf(planName).registrationPlanInterface.getEmailText();
    }

    public long getThePlanPrice() {
        return this.price;
    }

    public List<RegistrationPlanDTO> getUpdateOptions(){
        return this.registrationPlanInterface.getPossibleUpgradePlans();
    }
}
