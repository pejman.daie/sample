
public PaymentResponseDTO createPaymentSession(String planName) throws StripeException, BusinessServiceException {
        User currentUser = userService.getCurrentUserLoggedIn();
        RegistrationPlan registrationPlan = RegistrationPlan.valueOf(planName);
        long price = registrationPlan.getThePlanPrice();
        return addTempPlanAndCreateSession(currentUser, registrationPlan, price);
    }
