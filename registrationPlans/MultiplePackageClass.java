package com.loanthem.user.infra.registrationplans;

import com.loanthem.base.exceptions.BusinessServiceException;
import com.loanthem.payment.domain.dto.response.RegistrationPlanDTO;
import com.loanthem.user.domain.dto.UserRegistrationPlanDTO;
import com.loanthem.user.domain.enums.RegistrationPlan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MultiplePackageClass extends RegistrationPlansBaseClass {

    @Override
    public UserRegistrationPlanDTO validatePlan(UserRegistrationPlanDTO lastPlan) throws BusinessServiceException {
        checkThePlanToBeInActiveDuration(lastPlan);
        return lastPlan;
    }


    public List<RegistrationPlanDTO> getPossibleUpgradePlans() {
        RegistrationPlanDTO registrationPlanDTO = new RegistrationPlanDTO(RegistrationPlan.PREMIUM_PACKAGE, RegistrationPlan.PREMIUM_PACKAGE.getThePlanPrice() - RegistrationPlan.MULTIPLE_PACKAGE.getThePlanPrice());
        return Collections.singletonList(registrationPlanDTO);
    }
}
