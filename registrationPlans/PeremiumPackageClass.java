package com.loanthem.user.infra.registrationplans;

import com.loanthem.base.exceptions.BusinessServiceException;
import com.loanthem.payment.domain.dto.response.RegistrationPlanDTO;
import com.loanthem.user.domain.dto.UserRegistrationPlanDTO;
import com.loanthem.user.domain.enums.RegistrationPlan;

import java.util.ArrayList;
import java.util.List;

public class PeremiumPackageClass extends RegistrationPlansBaseClass {
    @Override
    public UserRegistrationPlanDTO validatePlan(UserRegistrationPlanDTO lastPlan) throws BusinessServiceException {
        checkThePlanToBeInActiveDuration(lastPlan);
        return lastPlan;
    }


}
