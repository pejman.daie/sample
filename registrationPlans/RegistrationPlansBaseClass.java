package ...

import base.exceptions.BusinessServiceException;
import base.exceptions.enums.ErrorCode;
import payment.domain.dto.response.RegistrationPlanDTO;
import user.domain.dto.UserRegistrationPlanDTO;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.http.HttpStatus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class RegistrationPlansBaseClass {

    public UserRegistrationPlanDTO validatePlan(UserRegistrationPlanDTO lastPlan) throws BusinessServiceException {
        throw new BusinessServiceException(HttpStatus.EXPECTATION_FAILED, ErrorCode.IO_EXCEPTION);
    }

    public abstract String getEmailText();

    void checkThePlanToBeInActiveDuration(UserRegistrationPlanDTO lastPlan) throws BusinessServiceException {
        Date startDate = lastPlan.getStartDate();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date incrementedDate = null;
        incrementedDate = DateUtils.addYears(startDate, 1);
        Date now = new Date();
        if (now.after(incrementedDate)) {
            throw new BusinessServiceException(HttpStatus.BAD_REQUEST, ErrorCode.NO_ACTIVE_PLAN);
        }
    }

    public List<RegistrationPlanDTO> getPossibleUpgradePlans() {
        return new ArrayList<>();
    }
}
