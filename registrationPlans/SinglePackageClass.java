package ....

import base.exceptions.BusinessServiceException;
import base.exceptions.enums.ErrorCode;
import payment.domain.dto.response.RegistrationPlanDTO;
import user.domain.dto.UserRegistrationPlanDTO;
import user.domain.enums.RegistrationPlan;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

public class SinglePackageClass extends RegistrationPlansBaseClass {

    @Override
    public UserRegistrationPlanDTO validatePlan(UserRegistrationPlanDTO lastPlan) throws BusinessServiceException {
        throw new BusinessServiceException(HttpStatus.BAD_REQUEST, ErrorCode.NO_ACTIVE_PLAN);
    }



    public List<RegistrationPlanDTO> getPossibleUpgradePlans() {
        List<RegistrationPlanDTO> registrationPlanDTOS = new ArrayList<>();
        RegistrationPlanDTO registrationPlanDTO = new RegistrationPlanDTO(RegistrationPlan.MULTIPLE_PACKAGE, RegistrationPlan.MULTIPLE_PACKAGE.getThePlanPrice());
        RegistrationPlanDTO registrationPlanDTO2 = new RegistrationPlanDTO(RegistrationPlan.PREMIUM_PACKAGE, RegistrationPlan.PREMIUM_PACKAGE.getThePlanPrice());
        registrationPlanDTOS.add(registrationPlanDTO);
        registrationPlanDTOS.add(registrationPlanDTO2);
        return registrationPlanDTOS;
    }
}
